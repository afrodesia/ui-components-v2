import React, { useContext } from 'react'
import { UserContext } from '../../../App'


export default function User() {
  const value =  useContext(UserContext)
  return (
    <div className="user">
      Hello, {value}
    </div>
  )
}


