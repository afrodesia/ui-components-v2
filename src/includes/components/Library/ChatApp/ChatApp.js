import React from 'react'

import { ChatInput } from './components/'

export default function ChatApp() {

  return (
    
      <div>
        <h2>Chat App</h2>
         <ChatInput/>
      </div>
  )
}