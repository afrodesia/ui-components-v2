import React from 'react'
import styled from 'styled-components'

const ChatInput  = styled.div`
  background:#222;
  padding:30px;
  input{
    width:100%;
    background:#222;
    color:#fff;
    padding:10px;
    border:none;
    border:1px solid #fff;
  }
` 


export default function Todos() {

  return (
      <ChatInput>
        <input type="text"/>
      </ChatInput>
  )
}