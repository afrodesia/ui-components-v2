import React, { useState } from 'react'
import styled from 'styled-components'

const TicTacArea = styled.div`
  .board-row:after {
    clear: both;
    content: "";
    display: table;
  }

  .status {
    margin-bottom: 10px;
  }

  .square {
    background: #fff;
    border: 1px solid #999;
    float: left;
    font-size: 29px;
    font-weight: bold;
    line-height: 100px;
    height: 100px;
    margin-right: -1px;
    margin-top: -1px;
    padding: 0;
    text-align: center;
    width: 100px;
  }

  .square:focus {
    outline: none;
  }

  .kbd-navigation .square:focus {
    background: #ddd;
  }

  .game {
    display: flex;
    flex-direction: row;
  }

  .game-info {
    margin-left: 20px;
  }
`

function Square({ value, onClick}) {

  return (
    <button
      onClick={onClick}
      className="square">
      {value}
    </button>
  )
}

function Board() {

  const [squares , setSquares ] = useState(Array(9).fill(null))
  const [ isXnext, setXTurn ] = useState(true)
    function renderSquare(i) {
      return( 
        <Square 
          onClick={() => {
            const nextSquares = squares.slice()
            nextSquares[i] = isXnext ? 'X' : 'O'
            setXTurn(!isXnext)
            setSquares(nextSquares)
          }}
          value={squares[i]}
           />
      )
    }

    const status = `Next player: X`;

    return (
      <div>
        <div className="status">{status}</div>
        <div className="board-row">
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div className="board-row">
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div className="board-row">
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>
      </div>
    )
}

function Game(){
  return (
    <div className="game">
        <div className="game-board">
          <Board />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
      </div>
    </div>
  )
}

function TicTack(){
  return(
    <TicTacArea>
      <div className="">
          <h2>Tic Tack</h2>
          <Game/>
      </div>
    </TicTacArea>
  )
}

export default TicTack