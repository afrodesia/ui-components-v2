import React, {Fragment, useState, useEffect } from 'react'
import styled from 'styled-components'

const Form = styled.form`
  .form-group{
    .form-control:focus{
      background: none;
      outline: none;
      color:green;
      border-color:#F5F5F5;
      border-bottom: 2px solid green;
    }
    label{
      color :#000;
      text-transform: uppercase;
      font-size: .8rem;
    }
    input{
      display: flex;
      width:100%;
      border:0px;
      background: none;
      padding: 3px;
      border-bottom:2px solid #303030;
      outline: 0px;
      margin:20px 0px 0px 0px;
      font-size: .7em; 
      border-radius: 0px;
    }
    .form-control.is-invalid{
      border-bottom:2px solid #dc3545;
    }
  }

`

export default function LooseForm() {

    const [name, setName ] = useState('Ken')
    const [surname, setSurname ] = useState('Torry')

    // const [ form, setForm ] = useState({
    //   firstname: '',
    //   surname: ''
    // })

    // const [input, setInput] = useEffect(null)

    const [width, setWidth ] = useState(window.innerWidth)

    useEffect(() => {
      const handleResize = () => setWidth(width.innerWidth)
      window.addEventListener('resize', handleResize)
      return () =>  window.removeEventListener('resize', handleResize)
    })

    // function handleChange(event) {
    //   setForm({
    //     ...form,
    //     [event.target.name]: event.target.value
    //   })
    // }

    function handleNameChange(e){
      setName(e.target.value)
    }

    function handleSurnameChange(e){
      setSurname(e.target.value)
    }

    return (
      <Fragment>
        <h2> Loose Form</h2>
        <Form>
        <div className="form-group">
        <input
          className="form-control"
          placeholder="Please provide a name"
          type="text"
          name="firstname"
          onChange={handleNameChange}
          />
        <label>Name</label>
      </div>

      <div className="form-group">
        <input
          className="form-control"
          placeholder="Please provide a surname"
          type="text"
          name="surname"
          onChange={handleSurnameChange}
          />
          <label>Surname</label>
      </div>
        <p>Hello, { name } {surname}</p>
        <p>Window width: {width}</p>
        </Form>
      </Fragment>
    )
  }
