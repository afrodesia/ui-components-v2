import React, { useState } from 'react'
import styled from 'styled-components'

const Form = styled.form`
width:50%;
  .form-group{
    .form-control:focus{
      background: none;
      outline: none;
      color:green;
      border-color:#F5F5F5;
      border-bottom: 2px solid green;
    }
    label{
      color :#000;
      text-transform: uppercase;
      font-size: .8rem;
    }
    input{
      display: flex;
      width:100%;
      border:0px;
      background: none;
      padding: 3px;
      border-bottom:2px solid #303030;
      outline: 0px;
      margin:20px 0px 0px 0px;
      font-size: .7em; 
      border-radius: 0px;
    }
    .form-control.is-invalid{
      border-bottom:2px solid #dc3545;
    }
  }

`
export default function Register() {

  const [ form, setForm ] = useState({
    username: '',
    email: '',
    password: ''

  })
  const [user, setUser] = useState(null)

  const handleChange = event => {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    })
  }
  const handleSubmit = event => {
    event.preventDefault()
    setUser(form)
  }
  
  return (
    <div>
      <h2>Register</h2>
      <Form onSubmit={handleSubmit}>
         <div className="form-group">
          <input
            className="form-control"
            placeholder="Please provide in a username"
            type="text"
            name="username"
            onChange={handleChange}
            />
          <label>username</label>
        </div>
        <div className="form-group">
          <input
            className="form-control"
            placeholder="Please type in a password"
            type="password"
            name="password"
            onChange={handleChange}
            />
            <label>Password</label>
        </div>
        <div className="form-group">
          <input
            className="form-control"
            placeholder="Please use a legit email address"
            type="email"
            name="email"
            onChange={handleChange}
            />
            <label>Email</label>
        </div>
          <button  className="black-btn" type="submit">Submit</button>
          <p>
          { user && JSON.stringify(user, null,  2)}
          </p>
      </Form>
    </div>
  )
}
