import React, { useEffect, useState, Fragment } from 'react'
import styled from 'styled-components'
import axios from 'axios'

const NewsArea = styled.section`
    .form-group{
    .form-control:focus{
      background: none;
      outline: none;
      color:green;
      border-color:#F5F5F5;
      border-bottom: 2px solid green;
    }
    label{
      color :#000;
      text-transform: uppercase;
      font-size: .8rem;
    }
    input{
      display: flex;
      width:100%;
      border:0px;
      background: none;
      padding: 3px;
      border-bottom:2px solid #303030;
      outline: 0px;
      margin:20px 0px 0px 0px;
      font-size: .7em; 
      border-radius: 0px;
    }
    .form-control.is-invalid{
      border-bottom:2px solid #dc3545;
    }
  }

  .loading{
    padding-top: 100px;
    text-align:center;
  }
`
const Article = styled.article`
  padding:20px 0;
  a{
    color:#333;
    font-weight:900;
    &:hover{
      color:yellow;
    }
  }
`
export default function News() {

  const [results , setResults] = useState([])
  const [query, setQuery ] = useState('react hooks')
  const [loading , setLoading] = useState(false)


  useEffect(() => {
    getResults()
      
  }, [])

  const getResults = async () => {

    setLoading(true)

    const res = await axios
      // .get(`https://www.dj.kentorry.io/admin/api/collections/get/Mixes?token=1661ae5030e1d70916a72e1bbc78ac=${query}`)
      .get(`http://hn.algolia.com/api/v1/search?query=${query}`)
        setResults(res.data.hits)
        // setResults(res.data.entries)
        console.log(res.data)
        setLoading(false)
  }

  const handleSearch = event => {
    event.preventDefault()
    getResults()
  }

  return (
    <NewsArea>
        <h2>React Hooks News !</h2>
        <form onSubmit={handleSearch}>
          <div className="form-group">
            <input 
              type="text"
              className="form-control" 
              value={query}
              onChange={event => setQuery(event.target.value)} 
            />
            <button 
              type="submit"
            >
            Go!
            </button>
            <label>Begin your seach!</label>
          </div>
        </form>
       
        {loading ? (
              <div className="loading">loading results..</div>
            ) : (
            <Fragment>
                  {results.map(result => (
                    <Article key={result.objectID}>
                      <a href={result.url} target="blank">
                        {result.title}
                    </a>
                    </Article> 
                ))}
            </Fragment>
        )}
       {/*  */}
    </NewsArea>
  )
}
