import React, { useMemo, useState } from 'react'


function useForm(){
  const Form  = useMemo(
    () => ({ children }) => <form>{children}</form>,
    []
  )
  return{
    Form
  }
}


function Dynamic() {

  const [ state, setState ] = useState(0)
  const { Form } = useForm()
  return (
    <div className="">
      <h2>Dynamic</h2>
      <Form>
        <input></input>
      </Form>
      <button 
        className="black-btn"
        onClick={() => setState(old => old + 1) }>{state}</button>
    </div>
  )
}

export default Dynamic
