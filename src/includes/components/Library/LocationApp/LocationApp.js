import React from 'react'
import Location from './components/Location'

export default function LocationApp() {
    return (
      <div>
        <h2>Location App</h2>
        <Location/>
      </div>
    )
  }
