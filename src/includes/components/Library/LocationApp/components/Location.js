import React, { Component } from 'react'
import SeasonDisplay from './SeasonDisplay'
import Loader from './Loader'

export default class Location extends Component{

  state = { lat: null, errMessage: '' }

  componentDidMount(){
    window.navigator.geolocation.getCurrentPosition(
      (position) =>  {
        this.setState({ 
          lat: position.coords.latitude
        })
      },
      (err) => {
        this.setState({
          errMessage: err.message
        })
      }
    )
  }
  componentDidUpdate(){
    console.log('Component did update')
  }

  renderContent(){
    if(this.state.errMessage && !this.state.lat){
      return <div>Error: {this.state.errMessage}</div>
    }
    if(!this.state.errMessage && this.state.lat){
      return <SeasonDisplay lat={this.state.lat} />
    }
    return <Loader message="Please allow us to know you location"/>
  }

  render() {
    return(
      <div>
        {this.renderContent()}
      </div>
    )
 
  }
}
