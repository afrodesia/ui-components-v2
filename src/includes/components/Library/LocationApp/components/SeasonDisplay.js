import React from 'react'
import styled from 'styled-components'

const SeasonBg = styled.div`

  .sun{
    background:orangered;
    padding:50px 0;
    text-align:center;
  }
  .frost{
    background:blue;
    text-align:center;
    padding:50px 0;
  }
`


const seasonConfig = {
  summer:  {
    info: 'Lets go outside',
    iconName: 'sun'
  },
  winter: {
    info: 'To cold outside',
    iconName: 'frost'
  }
}

const getSeason = (lat, month) => {
  if(month > 2 && month < 9){
    return lat > 0 ? 'summer' : 'winter'
  }else{
    return lat > 0 ? 'winter' : 'summer'
  }
}

const SeasonDisplay = ( props ) => {
   
    const season = getSeason(props.lat, new Date().getMonth())
    const {info, iconName} = seasonConfig[season]
    console.log(season)

    return (
      <SeasonBg>
        <div className={`${iconName}`}>
          {info}
        </div>
      </SeasonBg>
    )
  }
  export default SeasonDisplay

