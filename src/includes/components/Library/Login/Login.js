import React, { useState } from 'react'
import styled from 'styled-components'

const Form = styled.form`
  .form-group{
    .form-control:focus{
      background: none;
      outline: none;
      color:green;
      border-color:#F5F5F5;
      border-bottom: 2px solid green;
    }
    label{
      color :#000;
      text-transform: uppercase;
      font-size: .8rem;
    }
    input{
      display: flex;
      width:100%;
      border:0px;
      background: none;
      padding: 3px;
      border-bottom:2px solid #303030;
      outline: 0px;
      margin:20px 0px 0px 0px;
      font-size: .7em; 
      border-radius: 0px;
    }
    .form-control.is-invalid{
      border-bottom:2px solid #dc3545;
    }
  }

`
export default function Login() {

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [user, setUser] = useState(null)

  const handleSubmit = event => {
    event.preventDefault()
    const userData = {
      username,
      password,
    }
    setUser(userData)
    setUsername('')
    setPassword('')
  }
  return (
    <div>
      <h2>Login</h2>
      <Form onSubmit={handleSubmit}>
         <div className="form-group">
          <input
            className="form-control"
            placeholder="Username"
            onChange={ event => setUsername(event.target.value) } 
            type="text"
            value={username} 
            />
          <label>username</label>
        </div>
        <div className="form-group">
          <input
            className="form-control"
            placeholder="Password"
            onChange={ event => setPassword(event.target.value) } 
            type="text"
            value={password}  
            />
            <label>Password</label>
        </div>
          <button  className="black-btn" type="submit">Submit</button>
          <p>
          { user && JSON.stringify(user, null,  2)}
          </p>
         
      </Form>
    </div>
  )
}
