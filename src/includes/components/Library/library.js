import React from 'react'
import styled from 'styled-components'
import { addPrefetchExcludes } from 'react-static'
import { Link , Router} from '@reach/router'

import Dynamic from './Dynamic/Dynamic'
import Hooked from './Hooked/Hooked'
import TicTack from './TicTack/TicTack'
import Login from './Login/Login'
import Register from './Register/Register'
import Todos from './Todos/Todos'
import LibraryHome from './LibraryHome'
import News from './News/News'
import LooseForm from './LooseForm/LooseForm'
import Rwieruch from './Rwieruch/Rwieruch'
import FileUpload from './FileUpload/FileUpload'
import LocationApp from './LocationApp/LocationApp'
import Clock from './Clock/Clock'
import ChatApp from './ChatApp/ChatApp'


addPrefetchExcludes([
  'dynamic', 
  'tictack', 
  'library', 
  'hooked', 
  'login', 
  'register',
  'news',
  'todos',
  'loose',
  'rwieruch',
  'fileupload',
  'location-app',
  'clock',
  'chat'
])


const LibraryBg = styled.div`
  hgroup{
    margin-bottom:20px;
    padding-bottom:20px;
    border-bottom:1px solid #222;
  }
  main{
    display: grid;
      grid-template-columns:3fr .7fr;
      grid-gap:1em;
  }
  nav{
    background:#333;
    padding:20px;

    li{
      text-align:center;
      padding:10px 0px;
      
      a{
      color:#fff;
      cursor:pointer;
      width:100%;
        &:hover{
          color:#ccc;
        }
      }
    }
    .back-btn{
      background:#fff;
      color:#333;
      a{
        color:#333; 
      }
    }
  }
    
`
export default props => (
  <LibraryBg className="container">
      <hgroup>
         <h1>Library</h1>
      </hgroup>
      
      <main className="library-zone">
          <div className="content">
            <Router>
                <LibraryHome path="/"/>
                <Dynamic path="dynamic"/>
                <TicTack path="tictack"/>
                <Hooked path="hooked"/>
                <Login path="login"/>
                <Register path="register"/>
                <News path="news"/>
                <Todos path="todos"/>
                <LooseForm path="loose"/>
                <FileUpload path="fileupload"/>
                <Rwieruch path="rwieruch"/>
                <LocationApp path="location-app"/>
                <Clock path="clock"/>
                <ChatApp path="chat"/>
            </Router>
          </div>
          <nav>
            <ul>
        
              <li><Link to="tictack">tictack</Link></li>
              <li><Link to="dynamic">Dynamic</Link></li>
              <li><Link to="hooked">hooked</Link></li>
              <li><Link to="login">login</Link></li>
              <li><Link to="register">register</Link></li>
              <li><Link to="news">news</Link></li>
              <li><Link to="todos">todos</Link></li>
              <li><Link to="loose">loose form</Link></li>
              <li><Link to="rwieruch">rwieruch</Link></li>
              <li><Link to="fileupload">fileupload</Link></li>
              <li><Link to="location-app">location app</Link></li>
              <li><Link to="clock">clock</Link></li>
              <li><Link to="chat">chat app</Link></li>
              <li className="back-btn"><Link to="./"> -go back</Link></li>
             
            </ul>
            
          </nav>
        </main>          
  </LibraryBg>
)


