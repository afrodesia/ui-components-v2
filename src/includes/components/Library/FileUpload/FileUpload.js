import React, { useState }from 'react'
import styled from 'styled-components'


const Form = styled.form`
  .form-group{
    display:grid;
    grid-template-columns:2fr .2fr;
    .form-control:focus{
      background: none;
      outline: none;
      color:green;
      border-color:#F5F5F5;
      border-bottom: 2px solid green;
    }
    label{
      color :#000;
      text-transform: uppercase;
      font-size: .8rem;
    }
    input{
      display: flex;
      width:100%;
      border:0px;
      background: none;
      padding: 3px;
      border-bottom:2px solid #303030;
      outline: 0px;
      margin:20px 0px 0px 0px;
      font-size: .7em; 
      border-radius: 0px;
    }
    .form-control.is-invalid{
      border-bottom:2px solid #dc3545;
    }
    button{
      background:#222;
      color:#fff;
      border:0px;
      cursor: pointer;  
    }
  }
`

export default function FileUpload() {

  const [file, setFile ] = useState('')

  const onChange = e => {
    setFile(e.target.value)
    setFilename
  }
  
  return (
    <div>
      <h2>File Upload</h2>
      <Form>
        <div className="form-group">
          <input
            className="form-control"
            placeholder="Upload"
            type="file"
            />
            <label></label>
            <button type="submit ">upload</button>
        </div>
      
      
      </Form>
      
    </div>
  )
}