import React, { useState, useEffect } from 'react'

const initialLocationState = {
  latitude: null,
  longitude: null,
  speed: null
}


export default function Position(){

const [mousePosition, setMousePosition] = useState( { x: null, y: null})
const [status, setStatus] = useState(navigator.onLine)
const [location, setLocation] = useState(initialLocationState)


let mounted = true

useEffect(() => {
  window.addEventListener('mousemove', handleMouseMove)
  window.addEventListener('online', handleOnline)
  window.addEventListener('offline', handleOffline)

  navigator.geolocation.getCurrentPosition(handleGeoLocation)
  const watchId = navigator.geolocation.watchPosition(handleGeoLocation)

  return () => {
    window.removeEventListener('mousemove', handleMouseMove )
    window.removeEventListener('online', handleOnline)
    window.removeEventListener('offline', handleOffline)
    navigator.geolocation.clearWatch(watchId)
    mounted = false 
  }
}, [])


const handleGeoLocation = event => {

  if(mounted) {
    setLocation({
      latitude: event.coords.latitude,
      longitude: event.coords.longitude,
      speed: event.coords.speed
    })
  }
  
}

const handleOnline = () => {
  setStatus(true)
}
const handleOffline = () => {
  setStatus(false)
}

const handleMouseMove = event => {
  setMousePosition({
    x: event.pageX,
    y: event.pageY
  })
}

{
  return (
    <div>
       <h3>Mouse Position</h3>
       <p>
       {JSON.stringify(mousePosition, null, 2)}
       </p>
       

       <h3>Network Status </h3>
       <p>
        You are { status ? 'online' : 'offline'}
       </p>

       <h3>Geolocation</h3>
       <p>
        latitude is - { location.latitude }
       </p>
       <p>
       longitude is - { location.longitude }
      </p>
      <p>
      speed is - {location.speed ? location.speed : '0'}
      </p>
    </div>
  )
}
}
