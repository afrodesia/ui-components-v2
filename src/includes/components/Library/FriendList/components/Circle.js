import React from 'react';
import styled from 'styled-components'

const CircleArea  = styled.span`
  width:20px;
  height:20px;
  border-radius:100%;
` 

const Circle = () => {
  return (
    <CircleArea>
      
    </CircleArea>
  )
}

export default Circle