import React, { useState, useEffect , Fragment } from 'react'
import Circle from './components/Circle'

export default function FriendList() {

  const friendList = [

    {id: 1, name: 'Ken' },
    {id: 2, name: 'Mika' },
    {id: 2, name: 'Ariel' }

  ]

  function ChatRecipientPicker(){
    const [ recipientID, setRecipeientID ] = useState(1)
    const isRecipientOnline = useFriendStatus(recipientID)
    return (
      <Fragment>
        <Circle color={isRecipientOnline ? 'green' :'red'}/>
        <select
          value={recipientID}
          onChange={e => setRecipeientID(Number(e.target.value))}
        >
        {
          friendList.map(friend => (
            <option
              key={friend.id}
              value={friend.id}
              >
              {friend.name}
            </option>
          ))
        }
        </select>
      </Fragment>
    )
  }

  function useFriendStatus(friendID) {
    const [isOnline, setOnline ] = useState(null)
    const handleStatus = ( status ) => setIsOnline(status.isOnline)

    useEffect(() => {
      ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange)
      return () => {
        ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange)
      }
    })
    return isOnline
  }
  return(
    <Fragment>

    </Fragment>
  )
}

