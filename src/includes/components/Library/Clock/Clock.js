import React, { Component } from 'react'

export default class Clock extends Component {
  state = {time: new Date()}
  componentDidMount(){
    this.timerID = setInterval(() =>  this.tick(), 1000)
  }
    
 
  componentWillUnmount() {
   clearInterval(this.timerID)
  }

  tick(){
    this.setState({
      time: new Date()
    })
  }

  render() {
    return (
      <div>
        <h2>Clock</h2>
        The time is {this.state.time.toLocaleTimeString()} - {this.state.time.toLocaleDateString()}
      </div>
    )
  }
}
