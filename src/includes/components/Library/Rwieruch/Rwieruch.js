import React, { useState, useEffect } from 'react'
import axios from 'axios'

export default function Rwieruch() {

  const [ data, setData ] = useState({ hits: [] })
  
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        'http://hn.algolia.com/api/v1/search?query=redux',
      )
      setData(result.data)
    }
    fetchData()
  }, [])

  return (
    <div>
      <h2>Redux</h2>
      { data.hits.map(item => (
        <li key={item.objectID}>
          <a href={item.url}>
            {item.title}
          </a>
        </li>
      ))}
    </div>
  )
}
