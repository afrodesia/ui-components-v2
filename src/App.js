import React, {Fragment, } from 'react'
// import GlobalStyles from './globalStyles'
import { Root, Routes, addPrefetchExcludes } from 'react-static'
import {  Router } from 'includes/components/Router'
import Library from './includes/components/Library/library'
import Loader from './includes/components/Library/LocationApp/components/Loader'
addPrefetchExcludes(['library'])

export const UserContext = React.createContext()

const username = 'afrodesia'

import Nav from './includes/components/Nav/Nav'

import './app.css'
import './nav.css'

// Any routes that starts with 'dynamic' will be treated as non-static routes
function App() {
  return (
    <Fragment>
    <UserContext.Provider value={username}>
        <Root>
          <Nav/>
          <div className="content">
            <React.Suspense fallback={<Loader message="loading..."/>}>
              <Router>
                <Routes path="*" />
                <Library path="library/*"/>
              </Router>
            </React.Suspense>
          </div>
          </Root>
        </UserContext.Provider>
      </Fragment>
  )
}

export default App
