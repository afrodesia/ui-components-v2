import React from 'react'
import styled from 'styled-components'

const ErrorBg = styled.div`
  h1{
    color:#F7D8D8;
    font-size:3em;
    text-align:center;
    padding-top:100px;
  }
`


export default () => (
  <ErrorBg className="container">
    <h1>404 - Oh no's! We couldn't find that page :(</h1>
  </ErrorBg>
)
