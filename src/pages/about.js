import React from 'react'
import styled from 'styled-components'

const AboutBg = styled.div`
 
`

export default () => (
  <AboutBg className="container">
    <h1>About</h1>
    <p>a repository of unique react components that can bee used for your projects</p>
  </AboutBg>
)
