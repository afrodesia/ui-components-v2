import React from 'react'
import styled from 'styled-components'

const MastheadBg = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background:#222;
  color:#fff;
  height:84vh;
  .mast-content{
    text-align:center;
    color:#F7D8D8;
  }
  p,
  h1{
    padding:10px 0px;
  }
`

export default () => (
  <MastheadBg className="Masthead">
    <div className="mast-content">
      <h1>UI-Components</h1>
      <p>A repository of React Components.</p>
    </div>
  </MastheadBg>
)
