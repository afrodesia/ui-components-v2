import React from 'react'
import styled from 'styled-components'

import Masthead from './Home/Masthead'

const HomeBg = styled.div`
 
`

export default () => (
  <HomeBg>
    <Masthead/>
  </HomeBg>
)